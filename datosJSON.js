var DatosEstudiantes= [
  {
      apellido: "Bailon",
      nombre: "Arturo",
      semestre: "Cuarto",
      paralelo: "B",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"Arturo@correo.com"
  },
  {
      apellido: "Sans",
      nombre: "Sabina",
      semestre: "Quinto",
      paralelo: "B",
      direccion:"Los esteros",
      telefono:"0999999999",
      correo:"Sabina@correo.com"
  },
  {
      apellido: "Cardenas",
      nombre: "Cintia",
      semestre: "Sexto",
      paralelo: "A",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"Cintiao@correo.com"
  },
  {
      apellido: "Mosquera",
      nombre: "Isidro",
      semestre: "Primero",
      paralelo: "B",
      direccion:"Los esteros",
      telefono:"0999999999",
      correo:"Isidro@correo.com"
  },
  {
      apellido: "Barrera",
      nombre: "Judith",
      semestre: "Cuarto",
      paralelo: "A",
      direccion:"Miraflores",
      telefono:"0999999999",
      correo:"Judith@correo.com"
  },
  {
      apellido: "Araujo",
      nombre: "Elisabet",
      semestre: "Cuarto",
      paralelo: "B",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"Elisabet@correo.com"
  },
  {
      apellido: "Bailon",
      nombre: "Hector",
      semestre: "Cuarto",
      paralelo: "B",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"Hector@correo.com"
  },
  {
      apellido: "Millan",
      nombre: "Benjamin",
      semestre: "Quinto",
      paralelo: "B",
      direccion:"Las cumbres",
      telefono:"0999999999",
      correo:"Benjamin@correo.com"
  },
  {
      apellido: "Rueda",
      nombre: "Anton",
      semestre: "Cuarto",
      paralelo: "B",
      direccion:"Las cumbres",
      telefono:"0999999999",
      correo:"Anton@correo.com"
  },
  {
      apellido: "Parejo",
      nombre: "Aurelia",
      semestre: "Sexto",
      paralelo: "B",
      direccion:"Calle 13",
      telefono:"0999999999",
      correo:"Aurelia@correo.com"
  }
];



function CargarEstuduantes() {
  var estuSelect = document.getElementById('estu');
  var limite = DatosEstudiantes.length;
  for (var i = 0; i < limite; i++) {
      var nombrecompleto= DatosEstudiantes[i].apellido + " " + DatosEstudiantes[i].nombre;
      estuSelect.options[i+1]= new Option(nombrecompleto,nombrecompleto)    
  }

}

function CargarDatos(){
  var estuSelect = document.getElementById('estu').value;
  var limite = DatosEstudiantes.length;
  for (var i = 0; i < limite; i++) {
      var nombrecompleto= DatosEstudiantes[i].apellido + " " + DatosEstudiantes[i].nombre;
      if(estuSelect==nombrecompleto){
          document.getElementById("info").innerHTML = `
          <h4>Apellido</h4>
          ${DatosEstudiantes[i].apellido}
          <h4>Nombre</h4>
          ${DatosEstudiantes[i].nombre}
          <h4>Semestre</h4>
          ${DatosEstudiantes[i].semestre}
          <h4>Paralelo</h4>
          ${DatosEstudiantes[i].paralelo}
          <h4>Direccion</h4>
          ${DatosEstudiantes[i].direccion}
          <h4>Telefono</h4>
          ${DatosEstudiantes[i].telefono}
          <h4>Correo</h4>
          ${DatosEstudiantes[i].correo}
          `;
      }

  }
}
